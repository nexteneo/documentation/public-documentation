# OCPP 2.0.1 Specifications

- [Introduction](https://gitlab.infra.enexten.com/webstack/documentation/-/raw/master/ECPP/ECPP-v2/PDFs/OCPP-2.0.1_part0_introduction.pdf)
- [Architecture Topology](https://gitlab.infra.enexten.com/webstack/documentation/-/raw/master/ECPP/ECPP-v2/PDFs/OCPP-2.0.1_part1_architecture_topology.pdf)
- [Appendices](https://gitlab.infra.enexten.com/webstack/documentation/-/raw/master/ECPP/ECPP-v2/PDFs/OCPP-2.0.1_part2_appendices.pdf)
- [Specification](https://gitlab.infra.enexten.com/webstack/documentation/-/raw/master/ECPP/ECPP-v2/PDFs/OCPP-2.0.1_part3_specification.pdf)
- [OCPP-J](https://gitlab.infra.enexten.com/webstack/documentation/-/raw/master/ECPP/ECPP-v2/PDFs/OCPP-2.0.1_part4_ocpp-j-specification.pdf)
